FROM openjdk:17
ARG JAR_FILE=target/powermeter-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
#RUN apk add --no-cache tzdata
ENV TZ Asia/bangkok
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS  -jar app.jar" ]

