package me.pixka.powermeter.c

import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.transaction.Transactional

@RestController
class DeleteByDateControl(val ps:PowerValueService) {

    @PostMapping("/deletebydate")
    @CrossOrigin
    @Transactional
    fun deletebyDate(@RequestBody s:SearchObj): String {
        ps.deleteByDate(s.sdate!!,s.edate!!)
        return "{'result':'ok'}"
    }
}