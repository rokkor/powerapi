package me.pixka.powermeter.c

import me.pixka.powermeter.d.PowerValue
import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.SearchObj
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream

@RestController
class ExporttoExcel (val ps:PowerValueService){
    fun exports(datas: List<PowerValue>?): ResponseEntity<InputStreamResource> {
        val wb: Workbook = XSSFWorkbook()
        val sheet: Sheet = wb.createSheet("new sheet")
        val head: Row = sheet.createRow(0)
        createRow(head)
        var index = 1
        datas!!.forEach {
            var row = sheet.createRow(index)
            var cell = row.createCell(0)
            cell.setCellValue(index.toString())
            cell = row.createCell(1)
            cell.setCellValue(it.valuedate.toString())
            if (it.v12v != null) {
                cell = row.createCell(2)
                cell.setCellValue(it.v12v!!.toDouble())
            }
            if (it.v23v != null) {
                cell = row.createCell(3)
                cell.setCellValue(it.v23v!!.toDouble())
            }
            if (it.v31v != null) {
                cell = row.createCell(4)
                cell.setCellValue(it.v31v!!.toDouble())
            }
            cell = row.createCell(5)
            cell.setCellValue(it.i1a!!.toDouble())
            cell = row.createCell(6)
            cell.setCellValue(it.i2a!!.toDouble())
            cell = row.createCell(7)
            cell.setCellValue(it.i3a!!.toDouble())
            cell = row.createCell(8)
            cell.setCellValue(it.ptkw!!.toDouble())
            cell = row.createCell(9)
            cell.setCellValue(it.pft!!.toDouble())
            cell = row.createCell(10)
            cell.setCellValue(it.frqhz!!.toDouble())
            cell = row.createCell(11)
            cell.setCellValue(it.elmport!!.toDouble())
            cell = row.createCell(12)
            cell.setCellValue(it.rateaa!!.toDouble())
            cell = row.createCell(13)
            cell.setCellValue(it.counteraa!!.toDouble())
            cell = row.createCell(14)
            cell.setCellValue(it.gastempaa!!.toDouble())
            cell = row.createCell(15)
            cell.setCellValue(it.pressureaa!!.toDouble())
            cell = row.createCell(16)
            cell.setCellValue(it.velocityaa!!.toDouble())
            index++
        }

//        var f = FileOutputStream("testexportexcel.xlsx")
//        wb.write(f)
//        f.close()
        try {
            val bo = ByteArrayOutputStream()
            wb.write(bo)
            val respHeaders = HttpHeaders()
            respHeaders.contentType = MediaType.APPLICATION_FORM_URLENCODED
            respHeaders.contentLength = bo.size().toLong()
            respHeaders.setContentDispositionFormData("attachment", "export.xlsx")
            val isr = InputStreamResource(ByteArrayInputStream(bo.toByteArray()))
            return ResponseEntity(isr, respHeaders, HttpStatus.OK)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }


    }
    fun createRow(row: Row) {

        var cell = row.createCell(0)
        cell.setCellValue("#")
        cell = row.createCell(1)
        cell.setCellValue("name")
        cell = row.createCell(2)
        cell.setCellValue("v12v")
        cell = row.createCell(3)
        cell.setCellValue("v23v")
        cell = row.createCell(4)
        cell.setCellValue("v31v")
        cell = row.createCell(5)
        cell.setCellValue("i1a")
        cell = row.createCell(6)
        cell.setCellValue("i2a")
        cell = row.createCell(7)
        cell.setCellValue("i3a")
        cell = row.createCell(8)
        cell.setCellValue("ptkw")
        cell = row.createCell(9)
        cell.setCellValue("pft")
        cell = row.createCell(10)
        cell.setCellValue("frqhz")
        cell = row.createCell(11)
        cell.setCellValue("ealmport")
        cell = row.createCell(12)
        cell.setCellValue("rateaa")
        cell = row.createCell(13)
        cell.setCellValue("counteraa")
        cell = row.createCell(14)
        cell.setCellValue("gastempaa")
        cell = row.createCell(15)
        cell.setCellValue("pressureaa")
        cell = row.createCell(16)
        cell.setCellValue("velocityaa")
    }

    @CrossOrigin
    @PostMapping("/exporttoexcel")
    fun exporttoexcel(@RequestBody s:SearchObj): ResponseEntity<InputStreamResource> {

        var datas = ps.searchByDate(s.sdate!!,s.edate!!)

        return exports(datas)
    }
}