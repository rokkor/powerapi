package me.pixka.powermeter.c

import me.pixka.powermeter.d.PowerValue
import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.SearchObj
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ListdataControl (val pvs:PowerValueService){


    @CrossOrigin
    @PostMapping("/list")
    fun list(@RequestBody s:SearchObj): List<PowerValue>? {
        return pvs.searchByDate(s.sdate!!,s.edate!!)
    }

}