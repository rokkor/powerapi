package me.pixka.powermeter.c

import me.pixka.powermeter.d.ImportFileService
import me.pixka.powermeter.d.PowerValue
import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.findHeader
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.FileReader
import java.io.InputStreamReader
import java.io.Reader
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

@RestController
class UploadControl(val ps: PowerValueService,val ips: ImportFileService) {
    val fh = findHeader()
    val sdf = SimpleDateFormat("dd/MM/yy hh:mm:ss")
    fun getValue(n: String, record: CSVRecord): BigDecimal {
        var i = fh.getIndex(n)
        var value = record.get(i!!)
        if (value.toDoubleOrNull() != null) {
            return value.toBigDecimal()
        }
        println("Not found")
        return BigDecimal("0")
    }
    @CrossOrigin
    @PostMapping("/upload")
    fun upload(@RequestParam("afile") file: MultipartFile): Int {
        println("******** ${file.name} ")

        var ifs = ips.findOrCreate(file.name)

        val csv = CSVFormat.RFC4180.parse(InputStreamReader(file.inputStream))
        val records = csv.records
        fh.add(records.get(0))
        var sdate: Date? = null
        var edate: Date? = null
        var index = 0
        records.forEach {
            if (index > 0) {

                val r = PowerValue()
                var record = records[index]
                r.counteraa = getValue("CounterAA", record)
                r.rateaa = getValue("RateAA", record)
                r.i1a = getValue("i1A", record)
                r.i2a = getValue("i2A", record)
                r.i3a = getValue("i3A", record)
                r.ptkw = getValue("PTkW", record)
                r.pft = getValue("PFt", record)
                r.frqhz = getValue("FrqHz", record)
                r.elmport = getValue("EaImport", record)
                r.gastempaa = getValue("GasTempAA", record)
                r.pressureaa = getValue("PressureAA", record)
                r.velocityaa = getValue("VelocityAA", record)
                r.v12v = getValue("V12v", record)
                r.v23v = getValue("V23v", record)
                r.v31v = getValue("V31v", record)

                r.importFile = ifs

                var d = record[fh.find("dd/mm/yy")?.index!!]
                var t = record[fh.find("hh:mm:ss")?.index!!]

                var dt = "${d} ${t}"

                println(" DT ${dt}")

                r.adddate = sdf.parse(dt)
                r.valuedate = r.adddate

                if (sdate == null && edate == null)
                    sdate = sdf.parse(dt)
                edate = sdf.parse(dt)
                var v = ps.save(r)
                println(v)
            }
            index++
        }
//        return if (file.isEmpty) ResponseEntity(HttpStatus.NOT_FOUND) else ResponseEntity<String>(HttpStatus.OK)

        return index
    }
}