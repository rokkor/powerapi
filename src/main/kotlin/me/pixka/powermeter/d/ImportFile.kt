package me.pixka.powermeter.d

import me.pixka.powermeter.s.DefaultService
import me.pixka.powermeter.s.findByName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import javax.persistence.Entity

@Entity
class ImportFile(var name:String?=null):En() {
}

@Repository
interface ImportFileRepo:JpaRepository<ImportFile,Long>,findByName<ImportFile>



@Service
class ImportFileService(val r:ImportFileRepo):DefaultService<ImportFile>()
{
    @Synchronized
    fun findOrCreate(n:String): ImportFile {
        var f = findByName(n)
        if(f==null)
        {
            return save(ImportFile(n))
        }

        return f
    }
}

