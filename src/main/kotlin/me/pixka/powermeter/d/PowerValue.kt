package me.pixka.powermeter.d

import me.pixka.powermeter.s.DefaultService
import me.pixka.powermeter.s.search
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.ManyToOne

@Entity
class PowerValue(var name:String?=null,var valuedate: Date?=null,var v12v:BigDecimal?=null,var v23v:BigDecimal?=null,
var v31v:BigDecimal?=null,var i1a:BigDecimal?=null,var i2a:BigDecimal?=null,var i3a:BigDecimal?=null,
var ptkw:BigDecimal?=null,var pft:BigDecimal?=null,var frqhz:BigDecimal?=null,var elmport:BigDecimal?=null,
var rateaa:BigDecimal?=null,var counteraa:BigDecimal?=null,var gastempaa:BigDecimal?=null,var pressureaa:BigDecimal?=null,
var velocityaa:BigDecimal?=null,@ManyToOne var importFile: ImportFile?=null):En() {

    override fun toString(): String {
        return "id:${id} AddDate : ${adddate} "
    }
}

@Repository
interface PowerValueRepo:JpaRepository<PowerValue,Long>,search<PowerValue>
{
    @Query("from PowerValue pv where pv.name like %?1")
    override fun search(s: String, page: Pageable): List<PowerValue>?

    @Query("from PowerValue pv where  pv.adddate >= ?1 and pv.adddate <= ?2")
    fun searchbydate(s:Date,e:Date):List<PowerValue>?

    @Modifying
    @Query("delete from PowerValue pv where  pv.adddate >= ?1 and pv.adddate <= ?2")
    fun deleteByDate(s:Date,e:Date)
}


@Service
class PowerValueService(val r:PowerValueRepo):DefaultService<PowerValue>()
{
    fun searchByDate(s:Date,e:Date)=r.searchbydate(s,e)
    fun deleteByDate(s:Date,e:Date)=r.deleteByDate(s,e)
}
