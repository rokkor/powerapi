package me.pixka.powermeter.o

import org.apache.commons.csv.CSVRecord
import org.apache.poi.ss.usermodel.Row


class findHeader()
{
    var list  = ArrayList<Header>()

    fun find(n:String):Header?
    {
        return list.find { it.name.equals(n) }
    }
    fun getIndex(n:String): Int? {
       var f =  list.find { it.name.equals(n) }

        if(f!=null)
            return f.index

        return -1
    }
    fun add(r:Row)
    {
        var cells = r.iterator()

        cells.forEach {
            var found = find(it.stringCellValue)
            if(found==null)
            {
                var h = Header()
                h.index = it.columnIndex
                h.name = it.stringCellValue
                list.add(h)
            }
        }
    }
    fun add(r: CSVRecord)
    {
        var i = r.iterator()
        var index = 0

        i.forEach {
            var found = find(it)
            if(found==null)
            {
                var h = Header()
                h.index = index++
                h.name = it
                list.add(h)
            }
        }

    }
}
class Header(var name:String?=null,var index:Int?=null) {
    override fun toString(): String {
        return "name:${name} index:${index}"
    }
}