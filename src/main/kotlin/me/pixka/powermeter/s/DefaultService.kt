package me.pixka.powermeter.s

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

open class DefaultService<T> {

    @Autowired
    lateinit var repo: JpaRepository<T, Long>

    open fun delete(o: T) {
        repo.delete(o)
    }

    open fun deleteById(id: Long) = repo.deleteById(id)
    open fun all() = repo.findAll()

    open fun count() = repo.count()
    open fun save(o: T): T {
        return repo.save(o)
    }

    open fun findByName(n: String): T? {
        try {
            var i = repo as findByName<T>
            return i.findByName(n)
        } catch (e: Exception) {
            var message = "${e.message}  n:${n}"
            throw Exception(message)
        }
    }


}

interface findByName<T> {
    fun findByName(n: String): T?
}

interface search<T> {
    fun search(s: String, page: Pageable): List<T>?
}

interface findOrCreate<T> {
    fun findOrCreate(n: String): T
}