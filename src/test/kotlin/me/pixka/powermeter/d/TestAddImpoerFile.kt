package me.pixka.powermeter.d

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
class TestAddImpoerFile {
    @Autowired lateinit var imports:ImportFileService

    @Test
    fun testAddimportFile()
    {
        var ipf = ImportFile()
        ipf.name = "test import file"
        imports.save(ipf)

        Assertions.assertTrue(imports.all().size>0)
    }
}