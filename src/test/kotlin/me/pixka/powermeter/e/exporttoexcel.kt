package me.pixka.powermeter.e

import me.pixka.powermeter.d.ImportFileService
import me.pixka.powermeter.d.PowerValue
import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.Header
import me.pixka.powermeter.o.findHeader
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.io.FileReader
import java.io.Reader
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

@DataJpaTest
class exporttoexcel {


    val fh = findHeader()

    @Autowired
    lateinit var ps: PowerValueService

    @Autowired
    lateinit var ips: ImportFileService


    val sdf = SimpleDateFormat("dd/MM/yy hh:mm:ss")
    fun getValue(n: String, record: CSVRecord): BigDecimal {
        var i = fh.getIndex(n)
        var value = record.get(i!!)
        if (value.toDoubleOrNull() != null) {
            return value.toBigDecimal()
        }
//        println("Not found")
        return BigDecimal("0")
    }

    fun addToDb() {
        val i: Reader = FileReader("1.txt")
        val csv = CSVFormat.RFC4180.parse(i)

        var ifs = ips.findOrCreate("1.txt")
        val records = csv.records
        fh.add(records.get(0))
        var header = fh.list
        var sdate: Date? = null
        var edate: Date? = null
        var index = 0
        records.forEach {
            if (index > 0) {

                val r = PowerValue()
                var record = records[index]
                r.counteraa = getValue("CounterAA", record)
                r.rateaa = getValue("RateAA", record)
                r.i1a = getValue("i1A", record)
                r.i2a = getValue("i2A", record)
                r.i3a = getValue("i3A", record)
                r.ptkw = getValue("PTkW", record)
                r.pft = getValue("PFt", record)
                r.frqhz = getValue("FrqHz", record)
                r.elmport = getValue("EaImport", record)
                r.gastempaa = getValue("GasTempAA", record)
                r.pressureaa = getValue("PressureAA", record)
                r.velocityaa = getValue("VelocityAA", record)
                r.importFile = ifs

                var d = record[fh.find("dd/mm/yy")?.index!!]
                var t = record[fh.find("hh:mm:ss")?.index!!]

                var dt = "${d} ${t}"

                println(" DT ${dt}")

                r.adddate = sdf.parse(dt)
                r.valuedate = r.adddate

                if (sdate == null && edate == null)
                    sdate = sdf.parse(dt)
                edate = sdf.parse(dt)
                var v = ps.save(r)
                println(v)
            }
            index++
        }
        println("DATE: ${sdate}-${edate}")
        var found = ps.searchByDate(sdate!!, edate!!)
        println(found)
        Assertions.assertTrue(ps.all().size > 0)
        println("ALL ${ps.all().size}")
    }

    fun createRow(row: Row) {

        var cell = row.createCell(0)
        cell.setCellValue("#")
        cell = row.createCell(1)
        cell.setCellValue("name")
        cell = row.createCell(2)
        cell.setCellValue("v12v")
        cell = row.createCell(3)
        cell.setCellValue("v23v")
        cell = row.createCell(4)
        cell.setCellValue("v31v")
        cell = row.createCell(5)
        cell.setCellValue("i1a")
        cell = row.createCell(6)
        cell.setCellValue("i2a")
        cell = row.createCell(7)
        cell.setCellValue("i3a")
        cell = row.createCell(8)
        cell.setCellValue("ptkw")
        cell = row.createCell(9)
        cell.setCellValue("pft")
        cell = row.createCell(10)
        cell.setCellValue("frqhz")
        cell = row.createCell(11)
        cell.setCellValue("ealmport")
        cell = row.createCell(12)
        cell.setCellValue("rateaa")
        cell = row.createCell(13)
        cell.setCellValue("counteraa")
        cell = row.createCell(14)
        cell.setCellValue("gastempaa")
        cell = row.createCell(15)
        cell.setCellValue("pressureaa")
        cell = row.createCell(16)
        cell.setCellValue("velocityaa")
    }

    fun exports(datas: List<PowerValue>?) {
        val wb: Workbook = XSSFWorkbook()
        val sheet: Sheet = wb.createSheet("new sheet")
        val head: Row = sheet.createRow(0)
        createRow(head)
        var index = 1
        datas!!.forEach {
            var row = sheet.createRow(index)
            var cell = row.createCell(0)
            cell.setCellValue(index.toString())
            cell = row.createCell(1)
            cell.setCellValue(it.valuedate.toString())
            if (it.v12v != null) {
                cell = row.createCell(2)
                cell.setCellValue(it.v12v!!.toDouble())
            }
            if (it.v23v != null) {
                cell = row.createCell(3)
                cell.setCellValue(it.v23v!!.toDouble())
            }
            if (it.v31v != null) {
                cell = row.createCell(4)
                cell.setCellValue(it.v31v!!.toDouble())
            }
            cell = row.createCell(5)
            cell.setCellValue(it.i1a!!.toDouble())
            cell = row.createCell(6)
            cell.setCellValue(it.i2a!!.toDouble())
            cell = row.createCell(7)
            cell.setCellValue(it.i3a!!.toDouble())
            cell = row.createCell(8)
            cell.setCellValue(it.ptkw!!.toDouble())
            cell = row.createCell(9)
            cell.setCellValue(it.pft!!.toDouble())
            cell = row.createCell(10)
            cell.setCellValue(it.frqhz!!.toDouble())
            cell = row.createCell(11)
            cell.setCellValue(it.elmport!!.toDouble())
            cell = row.createCell(12)
            cell.setCellValue(it.rateaa!!.toDouble())
            cell = row.createCell(13)
            cell.setCellValue(it.counteraa!!.toDouble())
            cell = row.createCell(14)
            cell.setCellValue(it.gastempaa!!.toDouble())
            cell = row.createCell(15)
            cell.setCellValue(it.pressureaa!!.toDouble())
            cell = row.createCell(16)
            cell.setCellValue(it.velocityaa!!.toDouble())
            index++
        }

        var f = FileOutputStream("testexportexcel.xlsx")
        wb.write(f)
        f.close()
//        try {
//            val bo = ByteArrayOutputStream()
//            wb.write(bo)
//            val respHeaders = HttpHeaders()
//            respHeaders.contentType = MediaType.APPLICATION_FORM_URLENCODED
//            respHeaders.contentLength = bo.size().toLong()
//            respHeaders.setContentDispositionFormData("attachment", "export.xlsx")
//            val isr = InputStreamResource(ByteArrayInputStream(bo.toByteArray()))
//            return ResponseEntity(isr, respHeaders, HttpStatus.OK)
//        } catch (e: Exception) {
//            e.printStackTrace()
//            throw e
//        }


    }

    @Test
    fun export() {
        addToDb()
        var sd = sdf.parse("1/5/2022 00:00:00")
        var ed = sdf.parse("31/5/2022 00:00:00")

        var datas = ps.all()

        exports(datas)

    }

}