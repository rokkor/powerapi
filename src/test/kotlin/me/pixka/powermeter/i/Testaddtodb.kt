package me.pixka.powermeter.i

import me.pixka.powermeter.d.ImportFileService
import me.pixka.powermeter.d.PowerValue
import me.pixka.powermeter.d.PowerValueService
import me.pixka.powermeter.o.findHeader
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader
import java.io.Reader
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

@DataJpaTest
class Testaddtodb {
    val fh = findHeader()

    @Autowired
    lateinit var ps: PowerValueService

    @Autowired
    lateinit var ips: ImportFileService


    val sdf = SimpleDateFormat("dd/MM/yy hh:mm:ss")
    fun getValue(n: String, record: CSVRecord): BigDecimal {
        var i = fh.getIndex(n)
        var value = record.get(i!!)
        if (value.toDoubleOrNull() != null) {
            return value.toBigDecimal()
        }
        println("Not found")
        return BigDecimal("0")
    }

    @Test
    fun addToDb() {
        val i: Reader = FileReader("1.txt")
        val csv = CSVFormat.RFC4180.parse(i)

        var ifs = ips.findOrCreate("1.txt")
        val records = csv.records
        fh.add(records.get(0))
        var header = fh.list
        var sdate: Date? = null
        var edate: Date? = null
        var index = 0
        records.forEach {
            if (index > 0) {

                val r = PowerValue()
                var record = records[index]
                r.counteraa = getValue("CounterAA", record)
                r.rateaa = getValue("RateAA", record)
                r.i1a = getValue("i1A", record)
                r.i2a = getValue("i2A", record)
                r.i3a = getValue("i3A", record)
                r.ptkw = getValue("PTkW", record)
                r.pft = getValue("PFt", record)
                r.frqhz = getValue("FrqHz", record)
                r.elmport = getValue("EaImport", record)
                r.gastempaa = getValue("GasTempAA", record)
                r.pressureaa = getValue("PressureAA", record)
                r.velocityaa = getValue("VelocityAA", record)
                r.importFile = ifs

                var d = record[fh.find("dd/mm/yy")?.index!!]
                var t = record[fh.find("hh:mm:ss")?.index!!]

                var dt = "${d} ${t}"

                println(" DT ${dt}")

                r.adddate = sdf.parse(dt)
                r.valuedate = r.adddate

                if (sdate == null && edate == null)
                    sdate = sdf.parse(dt)
                edate = sdf.parse(dt)
                var v = ps.save(r)
                println(v)
            }
            index++
        }
        println("DATE: ${sdate}-${edate}")
        var found = ps.searchByDate(sdate!!, edate!!)
        println(found)
        Assertions.assertTrue(ps.all().size > 0)
        println("ALL ${ps.all().size}")
    }
}