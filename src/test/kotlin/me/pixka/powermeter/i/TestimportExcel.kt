package me.pixka.powermeter.i

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.io.File
import java.io.FileInputStream


@SpringBootTest
class TestimportExcel {



    @Test
    fun testreadFile()
    {
        val excelFile = FileInputStream(File("1.xlsx"))
        val workbook: Workbook = XSSFWorkbook(excelFile)
        val datatypeSheet: Sheet = workbook.getSheetAt(0)
        val iterator: Iterator<Row> = datatypeSheet.iterator()

        iterator.forEach { println(it) }
    }
}