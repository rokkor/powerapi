package me.pixka.powermeter.i

import me.pixka.powermeter.o.findHeader
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.io.FileReader
import java.io.Reader
import java.text.SimpleDateFormat

//@DataJpaTest
class TestreadCsv {

    val sd = SimpleDateFormat("dd/MM/yy hh:mm:ss")
    @Test
    fun testReadCSV()
    {
//        println(sd.parse("22/5/19"))
        val i: Reader = FileReader("1.txt")
        val records: Iterable<CSVRecord> = CSVFormat.RFC4180.parse(i)

        for (record in records) {
            val columnOne = record[0]
            val columnTwo = record[1]

            try {
                var d = sd.parse(columnTwo)
                    println(" ${d} ")
            }catch (E:Exception)
            {

            }
        }
    }
    @Test
    fun header()
    {
        val i: Reader = FileReader("1.txt")
        val records = CSVFormat.RFC4180.parse(i)
        var find = findHeader()
        var r = records.records
        find.add(r.get(0))

        println(find.list)

    }
}