package me.pixka.powermeter.i

import me.pixka.powermeter.o.Header
import me.pixka.powermeter.o.findHeader
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader
import java.io.Reader


class csvtoExcel {
    val fh = findHeader()

    fun createWB(header: ArrayList<Header>): Workbook {

        val wb: Workbook = XSSFWorkbook()
        val sheet: Sheet = wb.createSheet("new sheet")
        val createHelper = wb.creationHelper
        val row: Row = sheet.createRow(0)

        header.forEach {
            var cell = row.createCell(it.index!!)
            cell.setCellValue(it.name)
        }

        return wb


    }

    fun createRow(csvRecord: CSVRecord, row: Row, header: ArrayList<Header>) {
        header.forEach {

            var cell = row.createCell(it.index!!)
            cell.setCellValue(csvRecord.get(it.index!!))
        }

    }

    fun createDBRecored(csvRecord: CSVRecord, row: Row, header: ArrayList<Header>)
    {

    }

    @Test
    fun makeCSVtoExcel() {

        var file = FileOutputStream(File("out.xlsx"))
        val i: Reader = FileReader("1.txt")
        val csv = CSVFormat.RFC4180.parse(i)
        val records = csv.records
        fh.add(records.get(0))
        var header = fh.list
        var wb = createWB(header)
        var sheet = wb.getSheetAt(0)
        var index = 0
        records.forEach {

            if (index > 0) {
                var row = sheet.createRow(index)

                createRow(it, row, header)

            }
            index++

        }

        wb.write(file)
        file.close()
    }

}