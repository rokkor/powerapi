package me.pixka.powermeter.upload

import org.apache.commons.compress.utils.IOUtils
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileInputStream


@SpringBootTest
class TestUploadfile {
    @Autowired
    private val webApplicationContext: WebApplicationContext? = null

    @Test
    @Throws(Exception::class)
    fun whenFileUploaded_thenVerifyStatus() {

        val file = File("1.txt")
        val input = FileInputStream(file)
        val multipartFile = MockMultipartFile(
            "afile",
            file.getName(), "text/plain", IOUtils.toByteArray(input)
        )

        println(webApplicationContext)
        val mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext!!).build()


        mockMvc.perform(multipart("/upload").file(multipartFile)).andExpect(status().isOk())
    }
}